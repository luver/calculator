package calculator.tests;

import calculator.api.SolvingException;
import calculator.api.Solver;
import calculator.finiteStateMachineImpl.FiniteStateMachineSolver;
import org.junit.Test;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class SolvingTest {
    @Test
    public void SolvingTest() {
        Solver solver = new FiniteStateMachineSolver();
        try {
            double result = solver.solve("2*-3+10^2");
            System.out.println(result);
            assert result == 94.0;
            System.out.println(result == 94.0);
        } catch (SolvingException e) {
            System.out.print(e.getMessage() +" at: " +e.getErrorIndex());
        }
    }
}
