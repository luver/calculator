package calculator.finiteStateMachineImpl;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public enum State {
    START,
    NUMBER,
    OPERATOR
}
