package calculator.finiteStateMachineImpl.parsing;

import calculator.finiteStateMachineImpl.ParsingContext;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Parser {
    boolean parse(ParsingContext context);
}
