package calculator.finiteStateMachineImpl.parsing;

import calculator.finiteStateMachineImpl.ParsingContext;
import calculator.finiteStateMachineImpl.functions.Operator;

import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OperatorParser implements Parser {
    @Override
    public boolean parse(ParsingContext context) {
        Operator operator = getOperator(context);
        if (operator != null) {
            setOperator(operator, context);
            return true;
        }
        return false;
    }

    private void setOperator(Operator operator, ParsingContext context) {
        Deque<Operator> operatorsStack = context.getOperatorsStack();
        if (!operatorsStack.isEmpty()) {
            if (operator.getPriority().get() <= operatorsStack.getLast().getPriority().get()) {
                double result = operatorsStack.pop().exec(context.getArgumentsStack());
                context.getArgumentsStack().push(result);
            }
        }
        operatorsStack.push(operator);
    }
    OperatorFactory operatorFactory = new OperatorFactory();
    private Operator getOperator(ParsingContext context) {
        for (String possibleOperatorString : operatorFactory.getStrings()) {
            if (isOperator(possibleOperatorString, context)) {
                context.setCursor(context.getCursor() + possibleOperatorString.length());
                return operatorFactory.get(possibleOperatorString);
            }
        }
        return null;
    }

    private boolean isOperator(String operatorString, ParsingContext context) {
        return context.getExpression().indexOf(operatorString, context.getCursor()) == context.getCursor();
    }
}
