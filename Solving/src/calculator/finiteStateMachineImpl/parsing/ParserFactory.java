package calculator.finiteStateMachineImpl.parsing;

import calculator.finiteStateMachineImpl.State;

import java.util.HashMap;
import java.util.Map;

import static calculator.finiteStateMachineImpl.State.NUMBER;
import static calculator.finiteStateMachineImpl.State.OPERATOR;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ParserFactory {
    Map<State, Parser> parsers;

    public ParserFactory() {
        parsers = new HashMap<State, Parser>() {
            {
                put(NUMBER, new NumberParser());
                put(OPERATOR, new OperatorParser());
            }
        };

    }

    public Parser get(State state) {
        return parsers.get(state);
    }
}
