package calculator.finiteStateMachineImpl.parsing;

import calculator.finiteStateMachineImpl.ParsingContext;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class NumberParser implements Parser {
    @Override
    public boolean parse(ParsingContext context) {
        ParsePosition parsePosition = new ParsePosition(context.getCursor());
        NumberFormat numberFormat = new DecimalFormat();
        Number number = numberFormat.parse(context.getExpression(), parsePosition);

        if (parsePosition.getErrorIndex() == -1) {
            context.getArgumentsStack().push(number.doubleValue());
            context.setCursor(parsePosition.getIndex());
            return true;
        }
        return false;
    }
}
