package calculator.finiteStateMachineImpl.parsing;

import calculator.finiteStateMachineImpl.functions.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class OperatorFactory {
    final Map<String, Operator> operators;

    public OperatorFactory() {
        operators = new HashMap<String, Operator>() {
            {
                put("+", new PlusOperator());
                put("-", new MinusOperator());
                put("*", new MultiplyOperator());
                put("/", new DivideOperator());
                put("^", new PowerOperator());
            }
        };
    }

    Operator get(String operator) {
        return operators.get(operator);
    }

    Set<String> getStrings() {
        return operators.keySet();
    }
}
