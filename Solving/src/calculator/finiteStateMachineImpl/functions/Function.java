package calculator.finiteStateMachineImpl.functions;

import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Function {
    double exec(Deque<Double> args);

    Priority getPriority();

    int getArgumentsCount();
}
