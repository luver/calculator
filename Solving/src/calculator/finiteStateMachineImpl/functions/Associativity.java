package calculator.finiteStateMachineImpl.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public enum Associativity {
    LEFT,
    RIGHT
}
