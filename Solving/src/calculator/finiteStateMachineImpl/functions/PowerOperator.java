package calculator.finiteStateMachineImpl.functions;

import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PowerOperator implements Operator {
    @Override
    public double exec(double leftArgument, double rightArgument) {
        return Math.pow(leftArgument, rightArgument);
    }

    public double exec(Deque<Double> args) {
        double rightArgument = args.pop();
        double leftArgument = args.pop();
        return Math.pow(leftArgument, rightArgument);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    Priority priority = new Priority(3);

    @Override
    public int getArgumentsCount() {
        return 2;
    }

    @Override
    public Associativity getAssociativity() {
        return Associativity.LEFT;
    }
}
