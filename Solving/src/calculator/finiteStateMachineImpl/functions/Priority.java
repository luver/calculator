package calculator.finiteStateMachineImpl.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Priority {
    public Priority(int priority) {
        this.priority = priority;
    }
    public int get() {
        return priority;
    }
    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    final int priority;
    int order = 1;
}
