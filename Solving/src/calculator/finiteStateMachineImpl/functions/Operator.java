package calculator.finiteStateMachineImpl.functions;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Operator extends Function {
    double exec(double leftArgument, double rightArgument);

    Associativity getAssociativity();
}
