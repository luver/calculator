package calculator.finiteStateMachineImpl.functions;

import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class DivideOperator implements Operator {
    @Override
    public double exec(double leftArgument, double rightArgument) {
        return leftArgument / rightArgument;
    }

    @Override
    public double exec(Deque<Double> args) {
        double rightArgument = args.pop();
        double leftArgument = args.pop();
        return leftArgument / rightArgument;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    Priority priority = new Priority(2);

    @Override
    public int getArgumentsCount() {
        return 2;
    }

    @Override
    public Associativity getAssociativity() {
        return Associativity.LEFT;
    }
}
