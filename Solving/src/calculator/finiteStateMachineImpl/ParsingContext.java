package calculator.finiteStateMachineImpl;

import calculator.finiteStateMachineImpl.functions.Operator;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ParsingContext {

    public ParsingContext(String expression) {
        this.expression = expression;
        cursor = 0;
        isParsed = false;
    }

    public String getExpression() {
        return expression;
    }

    public boolean isParsed() {
        return isParsed;
    }

    public int getCursor() {
        return cursor;
    }

    boolean isParsed;

    public void setCursor(int cursor) {
        if (cursor >= expression.length()) isParsed = true;
        this.cursor = cursor;
    }

    int cursor;
    final String expression;

    public Deque<Double> getArgumentsStack() {
        return argumentsStack;
    }

    public Deque<Operator> getOperatorsStack() {
        return operatorsStack;
    }

    private Deque<Double> argumentsStack = new ArrayDeque<Double>();
    private Deque<Operator> operatorsStack = new ArrayDeque<Operator>();

    int currentPriorityOrder = 1;

    public void incCurrentPriorityOrder() {
        currentPriorityOrder++;
    }

    public void decCurrentPriorityOrder() {
        if (currentPriorityOrder > 1) {
            currentPriorityOrder--;
        } else {
            throw new RuntimeException("Something wrong with brackets");
        }

    }

}
