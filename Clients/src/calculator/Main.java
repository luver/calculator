package calculator;
import calculator.api.Solver;
import calculator.api.SolvingException;
import calculator.finiteStateMachineImpl.FiniteStateMachineSolver;

import java.util.Scanner;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Solver solver = new FiniteStateMachineSolver();
        try {
            System.out.println(solver.solve(scanner.next()));
        } catch (SolvingException e) {
            System.out.println(e.getErrorIndex());
        }
        System.out.println();
    }
}
